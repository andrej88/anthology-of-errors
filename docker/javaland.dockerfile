FROM eclipse-temurin:latest

SHELL [ "/bin/bash", "-c" ]
RUN apt-get update
RUN apt-get install --yes curl zip unzip
RUN curl -s https://get.sdkman.io | bash
RUN source /root/.sdkman/bin/sdkman-init.sh && sdk install kotlin && sdk install scala
RUN source /root/.sdkman/bin/sdkman-init.sh && ln -s $(which kotlinc) /usr/bin/kotlinc
RUN source /root/.sdkman/bin/sdkman-init.sh && ln -s $(which scalac) /usr/bin/scalac
