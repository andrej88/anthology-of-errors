FROM alpine:edge

# Zig is only in the "testing" repository of alpine:edge, which needs to be manually activated:
RUN echo "https://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories
RUN apk add zig
