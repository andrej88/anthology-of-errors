#!/usr/bin/env sh

HTML_OUTPUT_FOLDER_NAME="html/exhibits/fragments/$1"
mkdir -p "$HTML_OUTPUT_FOLDER_NAME"

# Arguments:
# 1. Tag of the docker image to run.
# 2. Command to run inside the container.
docker_run() {
    # shellcheck disable=SC2086
    docker run --rm --tty --volume ./sources:/sources "$1" $2
}

# Arguments:
# 1. Human-readable name of language and compiler, used as the output file name. Example: C (clang)
# 2. Tag of the docker image to run.
# 3. Command to run inside the container.
run_and_save_output_as_html() {
    docker_run "$2" "$3" | aha --stylesheet --no-header > "$HTML_OUTPUT_FOLDER_NAME/$1.html"
}

run_and_save_output_as_html "C (clang)"        anthology-of-errors/c        "clang -fsyntax-only /sources/$1/main.c"              &
run_and_save_output_as_html "C (gcc)"          anthology-of-errors/c        "gcc -fsyntax-only /sources/$1/main.c"                &
run_and_save_output_as_html "C++ (clang)"      anthology-of-errors/c        "clang -fsyntax-only /sources/$1/main.cpp"            &
run_and_save_output_as_html "C++ (g++)"        anthology-of-errors/c        "g++ -fsyntax-only /sources/$1/main.cpp"              &
run_and_save_output_as_html "C# (Mono csc)"    mono:latest                  "csc -nologo /sources/$1/main.cs"                     &
run_and_save_output_as_html "D (dmd)"          dlanguage/dmd:latest         "dmd /sources/$1/main.d"                              &
run_and_save_output_as_html "Go (go)"          golang:latest                "go build /sources/$1/main.go"                        &
run_and_save_output_as_html "Java (javac)"     anthology-of-errors/javaland "javac /sources/$1/main.java"                         &
run_and_save_output_as_html "Kotlin (kotlinc)" anthology-of-errors/javaland "kotlinc /sources/$1/main.kt"                         &
run_and_save_output_as_html "Rust (rustc)"     rust:latest                  "rustc /sources/$1/main.rs"                           &
run_and_save_output_as_html "Scala (scalac)"   anthology-of-errors/javaland "scalac -deprecation -explain /sources/$1/main.scala" &
run_and_save_output_as_html "Swift (swift)"    swift:latest                 "swift /sources/$1/main.swift"                        &
run_and_save_output_as_html "TypeScript (tsc)" anthology-of-errors/nodeland "tsc /sources/$1/main.ts"                             &
run_and_save_output_as_html "Zig (zig)"        anthology-of-errors/zig      "zig build-exe /sources/$1/main.zig"                  &

for PID in $(jobs -p); do
    wait "$PID"
done
