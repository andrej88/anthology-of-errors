#!/usr/bin/env sh

set -e

HTML_BODY_CONTENTS="<ul>\n"

for FILE in html/exhibits/*.html; do
    NAME=$(basename --suffix=.html "$FILE")
    HREF=$(echo "$FILE" | sed -E s#^html/##)
    INDEX_LINE="\t\t<li><a href=\"$HREF\">$NAME</a></li>\n"
    HTML_BODY_CONTENTS="${HTML_BODY_CONTENTS}${INDEX_LINE}"
done

HTML_BODY_CONTENTS="$HTML_BODY_CONTENTS\t</ul>"

sed -E "s#!!! REPLACE ME !!!#$HTML_BODY_CONTENTS#" < ./index.template.html > ./html/index.html
