#!/usr/bin/env sh

IS_MISSING_DEPENDENCIES=false

echo "If nothing is printed after this line, then there is nothing left to install."
command -v docker > /dev/null || { echo "Missing Docker"; IS_MISSING_DEPENDENCIES=true; }
command -v aha > /dev/null || { echo "Missing aha (Converts ASCII format codes into HTML)"; IS_MISSING_DEPENDENCIES=true; }
command -v sed > /dev/null || { echo "Missing sed"; IS_MISSING_DEPENDENCIES=true; }

if $IS_MISSING_DEPENDENCIES; then
    exit 1
else
    exit 0
fi
