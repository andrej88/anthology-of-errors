#!/usr/bin/env sh

set -e

THIS_SCRIPT_PATH=$(dirname "$0")

echo "Checking dependencies..."
"$THIS_SCRIPT_PATH/check_dependencies.sh"

echo "Building Docker images..."
"$THIS_SCRIPT_PATH/build_docker_images.sh"

start_job() {
    echo "Starting job for $NAME..."
    "$THIS_SCRIPT_PATH/run_all_compilers_for_one_error.sh" "$NAME"
    "$THIS_SCRIPT_PATH/compose_gallery_html_file.sh" "$NAME"
}

for FOLDER in sources/*; do
    NAME=$(basename "$FOLDER")
    start_job "$NAME" &
done

echo "Waiting for jobs to complete..."

for PID in $(jobs -p); do
    wait "$PID"
done

echo "Generating index.html..."
"$THIS_SCRIPT_PATH/compose_index_html_file.sh"

echo "Done."
