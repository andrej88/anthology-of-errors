#!/usr/bin/env sh

set -e

if [ -z "$1" ]; then
    echo "Missing input folder name as first argument"
    echo "Example usage:"
    echo "  ./make_gallery.sh typed_declaration_error"
    exit 1
fi

HTML_OUTPUT="<!DOCTYPE html>
<html>
  <head>
    <meta charset=\"UTF-8\">
    <title>$1</title>
    <link rel=\"stylesheet\" href=\"../gallery.css\">
  </head>
  <body>
    <dl>
"

for FILE in ./html/exhibits/fragments/"$1"/*; do
    NAME=$(basename --suffix=.html "$FILE")
    FILE_CONTENTS=$(cat "$FILE")

    # Dirty hack to get rid of the uninteresting parts of the Zig output. It
    # should always start with the file path in bold, so everything before that
    # is trash. This is normally "LLVM Emit Object" and random file names. I
    # couldn't find a flag to pass to Zig to suppress that so this will have to
    # do for now.
    if [ "$NAME" = "Zig (zig)" ]; then
      FILE_CONTENTS=$(echo "$FILE_CONTENTS" | sed -E 's#^.*(<span class="bold ">sources/)#\1#g')
    fi

    HTML_OUTPUT="$HTML_OUTPUT
      <dt>$NAME</dt>
      <dd><pre class=\"snippet\">$FILE_CONTENTS</pre></dd>
    "
done

HTML_OUTPUT="$HTML_OUTPUT
    </dl>
  </body>
</html>"

echo "$HTML_OUTPUT" > "./html/exhibits/$1.html"
