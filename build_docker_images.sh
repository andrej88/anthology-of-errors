#!/usr/bin/env sh

set -e

docker build --file ./docker/c.dockerfile --tag anthology-of-errors/c .
docker build --file ./docker/javaland.dockerfile --tag anthology-of-errors/javaland .
docker build --file ./docker/nodeland.dockerfile --tag anthology-of-errors/nodeland .
docker build --file ./docker/zig.dockerfile --tag anthology-of-errors/zig .
